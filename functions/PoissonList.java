package functions;

import models.Poisson;

public class PoissonList {    
    public static Poisson[] generate(int n, float miu) {
        Poisson[] poissons = new Poisson[n];

        int j = 0;
        float p = calculateP(0, miu);
        float F = p;
        poissons[0] = new Poisson(j, p, F);

        for(int i = 1; i < n; i++) {
            Poisson poissonBefore = poissons[i - 1];
            j = i;
            p = calculateNewP(j, poissonBefore.p, miu);
            F = poissonBefore.F + p;
            poissons[j] = new Poisson(j, p, F);
        }

        return poissons;
    }    

    static float calculateP(int j, float miu) {
        float result = (float)(Math.pow(Math.E, -miu) * Math.pow(miu, j) / factorial(j));
        return result;
    }

    static float calculateNewP(int j, float pBefore, float miu) {
        float result = (miu / j) * pBefore;
        return result;
    }

    static int factorial(int n) {
        int result = 1;
        for(int i = 1; i <= n; i++) {
            result *= i;
        }
        return result;
    }
}

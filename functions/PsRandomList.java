package functions;

import models.PsRandom;
import models.PsRandomP;
import models.Poisson;


public class PsRandomList {    
    public static PsRandom[] generate(int a, int c, int m, int x0, int n, int k) {
        PsRandom[] psRandom;
        psRandom = new PsRandom[n];

        psRandom[0] = new PsRandom(x0);
        for(int i = 1; i < n; i++) {
            int x = (a * psRandom[i - 1].x + c) % m;
            psRandom[i] = new PsRandom(x);
        }
        return psRandom;
    }

    public static PsRandomP[] generate(int a, int c, int m, int x0, int n, int k, Poisson[] poissons) {
        PsRandom[] psRandom = generate(a, c, m, x0, n, k);
        PsRandomP[] psRandomP = new PsRandomP[n];
                
        for(int i = 0; i < n; i++) {    
            int x = psRandom[i].x;
            float U = calculateU(x, m);
            int p = calculateP(U, poissons);
            psRandomP[i] = new PsRandomP(x, U, p);
        }

        return psRandomP;
    }

    static float calculateU(float x, int m) {
        // RUMUS YANG KITA GUNAKAN
        return (float) Math.pow((float) x / m, 2);

        // RUMUS YANG PAK HANDIKA GUNAKAN
        // return (float) x / m;
    }

    static int calculateP(float U, Poisson[] poissons) {
        int p = 0;
        for(int j = poissons.length - 1; j > 0; j--) {
            if(U >= poissons[j].F) {
                p = j + 1;
                break;
            }
        }
        return p;
    }
}

package functions;

import models.PsRandomP;
import models.Varian;

public class VarianList {
    public static Varian[] generate(PsRandomP[] psRandomPs) {
        Varian[] varians = new Varian[psRandomPs.length - 1];
        int k = 1;
        float xbar = psRandomPs[1].p;
        float s2 = 0;
        float s = 0;
        float sPerRootK = 0;
        varians[0] = new Varian(k, xbar, s2, s, sPerRootK);

        for (int i = 1; i < psRandomPs.length - 1; i++) {
            Varian varianB = varians[i - 1];
            PsRandomP psRandomPA = psRandomPs[i + 1];

            k = i + 1;
            xbar = varianB.xbar + ((psRandomPA.p - varianB.xbar) / k);
            s2 = (1 - (1 / (float) varianB.k)) * (float) varianB.s2 + (k * (float) (Math.pow(xbar - varianB.xbar, 2)));
            s = (float) Math.sqrt(s2);
            sPerRootK = s / (float) Math.sqrt(k);

            varians[i] = new Varian(k, xbar, s2, s, sPerRootK);
        }
        return varians;
    }
}

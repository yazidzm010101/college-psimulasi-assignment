# TUGAS PEMODELAN DAN SIMULASI

Repository ini saya gunakan untuk men-track perubahan selama saya mengerjakan tugas ini degan bahasa utama yang digunakan adalah bahasa java. Untuk menjalankan program dibutuhkan java compiler menggunakan Java Development Kit baik OpenJDK ataupun Oracle JDK.

# Struktur Program
Adapun secara garis besar struktur dari program adalah sebagai berikut.

  - App.java, digunakan sebagai main Class untuk menjalankan program sekaligus sebagai interface berupa console
  - package models(Folder models), digunakan untuk menyimpan skema/model dari data yang akan diolah.
  - package functions(Folder functions), berfungsi menyimpan berbagai macam handler/function untuk mengolah data yang ada dari model

### Menjalankan Program

```sh
$ javac App.java
$ java App
```
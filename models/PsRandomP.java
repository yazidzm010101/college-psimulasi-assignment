package models;

public class PsRandomP extends PsRandom {
    public float U;
    public int p;
    public PsRandomP(int x, float U, int p) {
        super(x);
        this.U = U;
        this.p = p;
    }
}

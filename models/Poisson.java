package models;

public class Poisson {
    public int j;
    public float p;
    public float F;

    public Poisson(int j, float p, float F) {
        this.j = j;
        this.p = p;
        this.F = F;
    }
}

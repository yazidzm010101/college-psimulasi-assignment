package models;

public class Varian {
    public int k;
    public float xbar;
    public float s2;
    public float s;
    public float sPerRootK;
    
    public Varian(int k, float xbar, float s2, float s, float sPerRootK) 
    {
        this.k = k;
        this.xbar = xbar;
        this.s2 = s2;
        this.s = s;
        this.sPerRootK = sPerRootK;
    }
}

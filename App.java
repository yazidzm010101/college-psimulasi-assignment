import functions.*;
import models.*;
import java.util.Scanner;

public class App 
{   
    // VARIABEL UTAMA
    Scanner scanner = new Scanner(System.in);
    Poisson[] poissons;
    PsRandomP[] psRandomPs;
    Varian[] varians;

    public void inputPoisson() {
        // VARIABEL UTAMA DALAM METHOD
        int n;
        float miu;                

        // CETAK JUDUL
        System.out.println("\n=== POISSON GENERATOR ===\n");

        // INPUT VARIABEL UTAMA
        System.out.print("Masukkan banyaknya data (n):");
        n = scanner.nextInt();        
        System.out.print("Masukkan nilai miu    (miu):");
        miu = scanner.nextFloat();
        
        // GENERATE POISSON
        poissons = PoissonList.generate(n, miu);

        // CETAK TABLE HEADER
        System.out.println("-".repeat(29));
        System.out.printf("| %-3s | %-8s | %-8s |\n", "j", "p(j)", "F(j)");
        System.out.println("-".repeat(29));        

        // CETAK TABLE BODY
        for (int i = 0; i < poissons.length; i++) {
            int j = poissons[i].j;
            float p = poissons[i].p;
            float F = poissons[i].F;
            System.out.printf("| %3d | %.6f | %.6f |\n", j, p, F);
        }        

        // CETAK TABLE FOOTER
        System.out.println("-".repeat(29));
    }

    public void inputPsRandomP() {
        // VARIABEL UTAMA
        int a, c, m, x0, n, k;

        // CETAK JUDUL
        System.out.println("\n=== PSEUDO RANDOM GENERATOR ===\n");

        // INPUT VARIABEL UTAMA
        System.out.print("Masukkan pengali       (a):");
        a = scanner.nextInt();
        System.out.print("Masukkan penambah      (c):");
        c = scanner.nextInt();
        System.out.print("Masukkan modulo        (m):");
        m = scanner.nextInt();
        System.out.print("Masukkan seed         (x0):");
        x0 = scanner.nextInt();
        System.out.print("Masukkan jumlah data   (n):");
        n = scanner.nextInt();
        System.out.print("Masukkan jumlah sampel (k):");
        k = scanner.nextInt();
        
        // GENERATE PSEUDO RANDOM YANG DIAPLIKASIKAN POISSON
        psRandomPs = PsRandomList.generate(a, c, m, x0, n, k, poissons);
        float sum = 0;
        float result = 0;

        // CETAK TABLE HEADER
        System.out.println("-".repeat(30));
        System.out.printf("| %-3s | %-4s | %-6s | %-4s |\n", "i", "x(i)", "u(i)", "p(i)");
        System.out.println("-".repeat(30));

        // CETAK TABLE BODY
        for (int i = 0; i < psRandomPs.length; i++) {
            int x = psRandomPs[i].x;
            float U = psRandomPs[i].U;
            int p = psRandomPs[i].p;
            sum += U;
            System.out.printf("| %3d | %4d | %.4f | %4d |\n", i, x, U, p);
        }
        result = sum / k;

        // CETAK TABLE FOOTER
        System.out.println("-".repeat(30));
        System.out.printf("Sum    = %f\n", sum);
        System.out.printf("Result = %f\n", result);
    }

    public void displayVarians()
    {
        varians = VarianList.generate(psRandomPs);

        // CETAK JUDUL
        System.out.println("\n=== VARIANS ===\n");

        // CETAK TABLE HEADER
        System.out.println("-".repeat(51));
        System.out.printf("| %-3s | %-8s | %-8s | %-8s | %-8s |\n", "k", "xbar", "s2", "s", "s/root_k");
        System.out.println("-".repeat(51));

        // CETAK TABLE BODY
        for (int i = 0; i < varians.length; i++) {
            int k = varians[i].k;
            String xbar = String.format("%.5f", varians[i].xbar);
            String s2 = String.format("%.5f", varians[i].s2);
            String s = String.format("%.5f", varians[i].s);
            String sPerRootK = String.format("%.6f", varians[i].sPerRootK);
            System.out.printf("| %3d | %8s | %8s | %8s | %8s |\n", k, xbar, s2, s, sPerRootK);
        }

        // CETAK TABLE FOOTER
        System.out.println("-".repeat(51));
    }

    // main METHOD UNTUK MENJALANKAN PROGRAM
    public static void main(String[] args) {
        App app = new App();    
        app.inputPoisson();
        app.inputPsRandomP();
        app.displayVarians();
    }
}